﻿using System.Web.Http;
using TestWebApi;
using TestWebApi.DependencyResolution;
using Microsoft.Owin;
using Owin;
using StructureMap;

[assembly: OwinStartup(typeof(Startup))]
namespace TestWebApi
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Configuration(app, IoC.Initialize());
        }

        public void Configuration(IAppBuilder app, IContainer container)
        {
            var config = new HttpConfiguration();
            config.DependencyResolver = new StructureMapWebApiDependencyResolver(container);
            WebApiConfig.Register(config);
            SwaggerConfig.Register(config);
            app.UseWebApi(config);
        }
    }
}
