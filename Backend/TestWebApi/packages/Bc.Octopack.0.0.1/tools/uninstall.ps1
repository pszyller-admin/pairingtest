param($installPath, $toolsPath, $package, $project)

$projectPath = $project.FullName;
$projectDirectory = (Get-Item $projectPath).DirectoryName
$projectDirectory
$projectName = $project.Name

$tmp = Get-Location
Set-Location $projectDirectory

$filePath = "$projectDirectory/$projectName.nuspec"

[IO.File]::Delete($filePath)

Set-Location $tmp