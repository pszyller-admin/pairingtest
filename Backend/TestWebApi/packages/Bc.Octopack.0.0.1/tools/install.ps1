param($installPath, $toolsPath, $package, $project)

$projectPath = $project.FullName;
$projectDirectory = (Get-Item $projectPath).DirectoryName
$projectDirectory
$projectName = $project.Name

$tmp = Get-Location
Set-Location $projectDirectory

$template = Get-Content (Join-Path $toolsPath -ChildPath "template.spec")
$template = $template -replace "{{ProjectName}}",$projectName
$filePath = "$projectDirectory/$projectName.nuspec"

[IO.File]::WriteAllText($filePath, $template)

Set-Location $tmp