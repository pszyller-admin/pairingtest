<?xml version="1.0" encoding="utf-8"?>
<package>
  <metadata>
    <id>{{ProjectName}}$BranchSuffix$</id>
    <title>{{ProjectName}}$BranchSuffix$</title>
    <version>$version$</version>
    <authors>British Council</authors>
    <description>{{ProjectName}}</description>
    <tags>{{ProjectName}}</tags>
    <projectUrl>https://britishcouncil.visualstudio.com/DefaultCollection</projectUrl>
    <dependencies>
    </dependencies>
  </metadata>
</package>